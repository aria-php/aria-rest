<?php

/** 
 * Example endpoint
 */

require_once(__DIR__ . '/app/vendor/autoload.php');

use ARIA\REST\{ router, APIDefinition, RESTEndpoint };

$router = router::router();

// Create an API Definition
$definition = new APIDefinition();

// Define endpoints - note, these would normally be in their own files, and nicely autoloaded

class ExampleEndpoint extends RESTEndpoint implements ARIA\REST\methods\GETEndpoint, \ARIA\REST\methods\POSTEndpoint {
  

  public function GET(array $args = []) {
    return 'GET called with ' . var_export($args, true) . var_export($this->request(), true);
  }

  public function POST(array $args = []) {
    return 'POST with ' . var_export($args, true) . var_export($this->request(), true);
  }

  public function registerGrants() {
    
  }

  public function registerAccess() {
    
  }

}

class ExampleEndpoint2 extends RESTEndpoint implements ARIA\REST\methods\GETEndpoint {
  

  public function GET(array $args = []) {
    return 'Second endpoint GET called with ' . var_export($args, true) . var_export($this->request(), true);
  }

  public function registerGrants() {
    
  }

  public function registerAccess() {
    
  }

}

class ExampleGrant extends RESTEndpoint implements ARIA\REST\methods\GETEndpoint {
  
  public function __construct() {
    $this->addGrantProvider(function($event) {
      
      $data = $event->data();
      
      if ($data['grant'] == 'example_grant') {
      
        // Do some sort of authentication here
        
        $event->response = true;
        
      }
    });
  }

  public function GET(array $args = []) {
    
    $this->grantGatekeeper('example_grant');
    
    return 'Granted!';
  }

  public function registerGrants() {
    
  }

  public function registerAccess() {
    
  }

}

class ReturnEcho extends RESTEndpoint implements ARIA\REST\methods\POSTEndpoint, ARIA\REST\methods\GETEndpoint {
  
  public function POST(array $args = array()) {
    return $this->request();
  }

  public function registerGrants() {
    
  }

  public function GET(array $args = array()) {
    return $this->request();
  }

  public function registerAccess() {
    
  }

}

class BearerEcho extends RESTEndpoint implements ARIA\REST\methods\GETEndpoint {
  
  public function GET(array $args = array()) {
    
    $this->grantGatekeeper('foo');
    $this->accessGatekeeper();
    
    return $this->token();
  }

  public function registerGrants() {
    
    $this->addGrantProvider(function($event) {
      $data = $event->data();
     
      if ($this->token()) {
        $event->response = true;
      }
      
    });
  }

  public function registerAccess() {
    $this->addAccessProvider(function($event) {
      $data = $event->data();
      
      if ($data['token']) {
        $event->response = [
            'id' => 1234,
            'username' => 'joebloggs'
        ];
      }
      
    });
  }

}

// Add some echos
$definition->addRoute('test/{slug}', ExampleEndpoint::class);
$definition->addRoute('different/{slug}', ExampleEndpoint2::class);
$definition->addRoute('granted', ExampleGrant::class);

$definition->addRoute('echo', ReturnEcho::class);
$definition->addRoute('bearerecho', BearerEcho::class);

// Add routes
$router->addAPI($definition);


// Serve routes 
$router->serve();