<?php

namespace ARIA\REST\auth;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Grants management.
 * 
 * Provide helpers to check permissions (note, how these permissions are actually checked and granted
 * are left as an exercise for the reader).
 * 
 * This handles AuthZ
 */
trait Grants {

  private static $grantDispatcher;

  /**
   * Has the named grant been issued in the current auth context.
   * Return true or false depending on whether the current auth context has the current grant permission, where
   * auth context may be a user or a machine context established by bearer token or other means.
   * @param string $grant
   * @return bool
   */
  public function hasGrant(string $grant) : bool {
    
    // No grant providers registered, so this can only ever be false
    if (empty(self::$grantDispatcher)) {
      return false;
    }
    
    $event = new GrantEvent($grant, $this);
    
    $event->response = false;
    self::$grantDispatcher->dispatch($event, 'ARIA/REST/auth/grants');
    
    return $event->response;
    
  }
  
  /**
   * Add a grant provider, which is a callable function that accepts a Symfony event dispatcher Event
   * @param callable $provider
   */
  public function addGrantProvider(callable $provider) {
    
    if (empty(self::$grantDispatcher)) {
      self::$grantDispatcher = new EventDispatcher();
    }
    
    self::$grantDispatcher->addListener('ARIA/REST/auth/grants', $provider, 0);
    
  }

  /**
   * Enforce a given grant context, throwing a SecurityException if it fails
   * @param string $grant
   * @throws SecurityException
   */
  public function grantGatekeeper(string $grant) {
    
    if (!$this->hasGrant($grant)) {
      throw new SecurityException("Grant {$grant} is not available in the current security context.");
    }
  }
  
  /**
   * Implement to register your client grants. 
   * Register your auth grants here, it's ok if this does nothing.
   */
  abstract public function registerGrants(); 
  
}
