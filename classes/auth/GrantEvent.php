<?php

namespace ARIA\REST\auth;

use Symfony\Contracts\EventDispatcher\Event;
use ARIA\REST\RESTEndpoint;

class GrantEvent extends Event {
  
  private $data = [];
  
  public function __construct(string $expected, RESTEndpoint $controller) {
    $this->data['grant'] = $expected;
    $this->data['controller'] = $controller;
  }
  
  public function &data() : array {
    return $this->data;
  } 
}