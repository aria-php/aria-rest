<?php

namespace ARIA\REST\auth;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provide AuthN methods
 */
trait Access {
  
  private static $accessDispatcher;
  
  /** 
   * Retrieve a bearer authentication token if present
   * @return string|null
   */
  protected function bearerToken(): ?string {

    $headers = null;
    $serverheaders = $this->headers();
    
    if (isset($serverheaders['Authorization']))
      $headers = trim($serverheaders["Authorization"]);
    else if (isset($serverheaders['HTTP_AUTHORIZATION']))
      $headers = trim($serverheaders["HTTP_AUTHORIZATION"]);

    if (!empty($headers)) {
      if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
        return trim($matches[1], '\'"');
      }
    }

    return null;
  }
  
  /**
   * Retrieve access token
   * @return string|null
   */
  protected function accessToken(): ?string {
    if ($access_token = $this->request('access_token')) {
      return $access_token;
    }
    
    return null;
  }

  /**
   * Retrieve an access token, if present
   * @return string|null
   */
  public function token(): ?string {
    return $this->bearerToken()??$this->accessToken();
  }
  
  
  /**
   * Add a access provider, which is a callable function that accepts a Symfony event dispatcher Event
   * @param callable $provider
   */
  public function addAccessProvider(callable $provider) {
    
    if (empty(self::$accessDispatcher)) {
      self::$accessDispatcher = new EventDispatcher();
    }
    
    self::$accessDispatcher->addListener('ARIA/REST/auth/access', $provider, 0);
    
  }
  
  /**
   * Trigger an event to ask whether the provided credentials grant access (as determined by your implementation system), and if so, to return
   * the associated user object.
   * @return false|access details (e.g. a user)
   */
  public function hasAccess() {
    if (empty(self::$accessDispatcher)) {
      return false;
    }
    
    $event = new AccessEvent($this->token()??'', $this);
    
    $event->response = false;
    self::$accessDispatcher->dispatch($event, 'ARIA/REST/auth/access');
    
    return $event->response;
  }
  
  /**
   * Enforce that a user has been authenticated to connect to connect to the API (a user has been found matching the token, or some other mechanism, but not what grants they have)
   * @throws SecurityException
   */
  public function accessGatekeeper() {
    
    if (!$this->hasAccess()) {
      throw new SecurityException("No user could be found matching credentials presented");
    }
  }
  
  /**
   * Implement to register your client grants. 
   * Register your auth grants here, it's ok if this does nothing.
   */
  abstract public function registerAccess(); 
}
