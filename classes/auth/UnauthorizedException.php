<?php

namespace ARIA\REST\auth;

class UnauthorizedException extends SecurityException {}