<?php

namespace ARIA\REST\auth;

use Symfony\Contracts\EventDispatcher\Event;
use ARIA\REST\RESTEndpoint;

class AccessEvent extends Event {
  
  private $data = [];
  
  public function __construct(string $token, RESTEndpoint $controller) {
    $this->data['token'] = $token;
    $this->data['controller'] = $controller; 
  }
  
  public function &data() : array {
    return $this->data;
  } 
}