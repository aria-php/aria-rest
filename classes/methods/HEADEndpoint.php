<?php

namespace ARIA\REST\methods;

/**
 * Interface that defines a specific REST method
 * @author marcus
 */
interface HEADEndpoint extends HTTPVerb {
 
    public function HEAD(array $args = []) : void;
    
}

