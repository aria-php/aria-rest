<?php

namespace ARIA\REST\methods;


/**
 * Interface that defines a specific REST method
 * @author marcus
 */
interface PUTEndpoint extends HTTPVerb {
 
    public function PUT(array $args = []);
    
}

