<?php

namespace ARIA\REST\methods;

/**
 * Interface that defines a specific REST method
 * @author marcus
 */
interface PATCHEndpoint extends HTTPVerb {
 
    public function PATCH(array $args = []);
    
}

