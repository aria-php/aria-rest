<?php

namespace ARIA\REST\methods;


/**
 * Interface that defines a specific REST method
 * @author marcus
 */
interface GETEndpoint extends HTTPVerb {
 
    public function GET(array $args = []);
    
}

