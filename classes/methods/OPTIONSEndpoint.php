<?php

namespace ARIA\REST\methods;


/**
 * Interface that defines a specific REST method
 * @author marcus
 */
interface OPTIONSEndpoint extends HTTPVerb {
 
    public function OPTIONS(array $args = []);
    
}

