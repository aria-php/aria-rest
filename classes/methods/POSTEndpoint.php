<?php

namespace ARIA\REST\methods;


/**
 * Interface that defines a specific REST method
 * @author marcus
 */
interface POSTEndpoint extends HTTPVerb {
 
    public function POST(array $args = []);
    
}

