<?php

namespace ARIA\REST\methods;

/**
 * Interface that defines a specific REST method
 * @author marcus
 */
interface DELETEEndpoint extends HTTPVerb {
 
    public function DELETE(array $args = []);
    
}

