<?php

namespace ARIA\REST;

/**
 * Interface that defines a valid rest endpoint.
 * Note, you should implement one or more of the methods/*, implementing this on its own will 
 * push you to enter some housekeeping information but won't actually implement an endpoint.
 * @author marcus
 */
abstract class RESTEndpoint implements methods\OPTIONSEndpoint {

  // Mix in Grants helpers (AuthZ)
  use \ARIA\REST\auth\Grants;
  
  // Mix in bearer handlers (AuthN)
  use \ARIA\REST\auth\Access;
  
  // Mix in data inputs
  use \ARIA\REST\data\Inputs;
  
  /**
   * Parent constructor, registering various necessaries
   */
  public function __construct() {
    $this->registerGrants();
    $this->registerAccess();
    $this->hasAccess();
  }
  
  /**
   * Detect methods that are implemented by this handler
   */
  protected function setMethodHeaders() {
    
    $methods = [];
    
    if ($this instanceof methods\GETEndpoint) {
      $methods[] = 'GET';
    }
    if ($this instanceof methods\DELETEEndpoint) {
      $methods[] = 'DELETE';
    }
    if ($this instanceof methods\HEADEndpoint) {
      $methods[] = 'HEAD';
    }
    if ($this instanceof methods\OPTIONSEndpoint) {
      $methods[] = 'OPTIONS';
    }
    if ($this instanceof methods\PATCHEndpoint) {
      $methods[] = 'PATCH';
    }
    if ($this instanceof methods\POSTEndpoint) {
      $methods[] = 'POST';
    }
    if ($this instanceof methods\PUTEndpoint) {
      $methods[] = 'PUT';
    }
    
    if (!empty($methods)) {
      header('Access-Control-Allow-Methods: ' . implode(', ', $methods) );
      header('Access-Control-Max-Age: 86400');
    }
  }
  
  /**
   * Implement a default OPTIONS handler for all endpoints in order to handle CORS preflight
   * @param array $args
   */
  public function OPTIONS(array $args = []) {
    
    http_response_code(204);
    $this->setMethodHeaders();
    
  }
  
}
