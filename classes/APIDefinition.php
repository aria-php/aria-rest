<?php

namespace ARIA\REST;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * API Definition.
 * This defines a particular version of an API
 */
class APIDefinition {
  
  /**
   * Base URL 
   */
  private $base = "";
  
  /**
   * List of handlers
   */
  private $handlers = [];

  /**
   * Construct an api definition (collection of apis)
   * @param string $urlbase Base of any endpoints in this collection e.g. 'api/user/'
   * @param string $version Optional version of this endpoint collection.
   */
  public function __construct(string $urlbase = 'api', string $version = '') {

    $this->base = trim( trim($urlbase, ' /') . '/' . trim($version, ' /'), '/') . '/'; // normalise base url
    
  }
  
  
  /**
   * Add a route to the definition.
   * @param string $definition Path definition
   * @param string $restControllerClass Handling class (note, we use a string here so we don't have to instantiate until we actually call the endpoint)
   * @param array $requirements Requirements for this definition, including matching strings
   * @param Route $route Optional partially completed Symfony route object, allowing you to pass advanced configuration
   */
  public function addRoute(string $definition, string $restControllerClass, array $requirements = [], Route $route = null) { 
     
    if (class_exists($restControllerClass)) {
      
      $definition = trim($definition, ' /'); // Normalise - remove prefixed and trailing '/'
      
      if (empty($route)) {
        $route = new Route($definition, ['_controller' => $restControllerClass]);
      }
      $route->setPath($definition);
      $route->setDefault('_controller', $restControllerClass);
      $route->setRequirements($requirements);
      
      $this->handlers[$definition] = $route;
      
    } else {
      
      throw new \RuntimeException("Handling class ($restControllerClass) for route $definition could not be found or is invalid.");
      
    }
      
  }
  
  /**
   * Retrieve a namespaced routing collection for this API
   * @return RouteCollection
   */
  public function getCollection() : RouteCollection {
    
    
    $collection = new RouteCollection();
    
    foreach ($this->handlers as $definition => $handler) {
    
      $collection->add($definition, $handler);
      
    }
    
    $collection->addPrefix($this->base);
    $collection->addNamePrefix($this->base);
    
    return $collection;
    
  }
  
}
