<?php

namespace ARIA\REST\data;

trait Inputs {

  /**
   * Define default signature hash algorithm
   */
  var $algorithm = 'sha256';

  /**
   * Input data array
   */
  private $data = [];
  private $raw;

  /**
   * Retrieve input variables (GET/POST/etc) from the data array
   * @param string $variable A specific variable to return, or if missing, the entire data array will be returned.
   * @return mixed|array Returns the named variable, if present, or the full data array if no value is given
   */
  public function request(string $variable = null) {
    
    if (empty($this->data)) {
      $this->processInputs();
    }

    if (!empty($variable)) {
      return $this->data[$variable];
    }

    return $this->data;
  }

  /**
   * Retrieve the raw data body
   * @return string Returns the request body in a raw string format
   */
  public function rawBody() {
    
    if (empty($this->raw)) {
      $this->processInputs();
    }

    return $this->raw;
  }

  /**
   * Get client request headers.
   * Retrieve all client request headers, note that this should be used instead of getallheaders(), which isn't
   * always available. 
   * @param $header Optional header to return.
   * @return array|string The specific header interested in, or the whole header array of not present
   */
  public function headers(string $header = null) {

    $headers = [];

    foreach ($_SERVER as $name => $value) {
      if (substr($name, 0, 14) == 'REDIRECT_HTTP_') {
        $name = substr($name, 9);
      }
      
      if (substr($name, 0, 5) == 'HTTP_') {
        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
      }
    }
    
    if (!empty($header)) {
      return $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('-', ' ', $header))))];
    }

    return $headers;
  }

  /**
   * Validate generated signature in POST
   * Algorithm used to hash the string can be modified in $this->algorithm
   *
   * @param string $key       Private key used to encrypt the signature
   * @param string $signature Signature string
   * @return void
   */
  public function validateSignature(string $key, string $signature){
    return $signature == base64_encode(hash_hmac($this->algorithm, $this->rawBody(), $key, true));
  }

  /**
   * Attempt to process inputs
   */
  protected function processInputs() {

    // Always add GET vars
    $this->data = array_merge($this->data, $_GET);

    // Now see if we need to parse any JSON content
    $contenttype = $this->headers('content-type'); // What payload are we dealing with?
    if (empty($contenttype)) {
      $contenttype = $_SERVER['CONTENT_TYPE']; // Alternative approach
    }
    
    if (strtolower($contenttype) == 'application/json') {
      
      // This is a json request. First, let's see if we've been sent anything in form input
      if (!empty($_REQUEST['json'])) {
        
        $json = trim($_REQUEST['json']);
        $json = str_replace('[]"', '"', $json); // Fake PHP's array conversion
        
        if ($parsed = @json_decode($json, true)) {
          $this->data = array_merge($parsed, $this->data());
        }
      }

      if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] != 'GET') {
        
        $body = @file_get_contents('php://input');
        $this->raw = $body;
        $body = trim($body);
        $body = str_replace('[]"', '"', $body); // Fake PHP's array conversion

        if (!empty($body)) {
          if ($parsed = @json_decode($body, true)) {
            $this->data = array_merge($parsed, $this->data);
          }
        }
      }
    } else {

      // Regular encoding, handle for various method types
      switch ($_SERVER['REQUEST_METHOD']) {
        
        case 'POST': 
          $this->data = array_merge($this->data, $_POST); // POST, easy peasy 
        break;
        case 'PUT' :
        case 'PATCH':
          // PATCH & PUT, we need to be a little smarter as it's not automatically populated by PHP
          
          parse_str(file_get_contents('php://input'), $patch);
          if (is_array($patch)) {
            $this->data = array_merge($this->data, $patch);
          }
        break;
        
      }
      
    }
  }

}
