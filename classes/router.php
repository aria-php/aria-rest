<?php

namespace ARIA\REST;

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\HttpFoundation\Request;
use ARIA\REST\methods\HTTPVerb;
use ARIA\REST\auth\{ SecurityException, UnauthorizedException };

/**
 * REST Router.
 * 
 * Serves the API Routes registered via apis and api endpoints
 */
class router {

  /**
   * Singleton for the page router
   */
  private static $router;
  
  /**
   * List of api definitions.
   */
  private $rootCollection = [];
  
  /**
   * Whitelist of permitted origins
   */
  private $originWhitelist = [];
  
  
  public function __construct() {
    $this->rootCollection = new RouteCollection();
  }
  
  /**
   * Add an API definition to this router.
   * @param \ARIA\REST\APIDefinition $api
   */
  public function addAPI(APIDefinition $api) {
    
    $this->rootCollection->addCollection($api->getCollection());
    
  }
  
  /**
   * Set permitted origins.
   * 
   * Accepts an array of regex domain definitions for matching domains.
   * @param array $origins
   */
  public function setAllowedOrigins(array $origins) {
    
    $this->originWhitelist = $origins;
    
  }
  
  /**
   * Using the whitelist of origins (or default *) set the CORS origin headers.
   */
  protected function originHeaders() {
    
    if (empty($this->originWhitelist)) {
      
      header("Access-Control-Allow-Origin: *");
      
    } else {
      
      $origin = $_SERVER['HTTP_ORIGIN'];
      foreach ($this->originWhitelist as $domain) {
        
        $domain = trim($domain, '/'); // some basic normalisation
        
        if (preg_match('/'.$domain.'/', $origin)) {
          
          header("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN']);
          
        }
        
      }
      
      
    }
    
  }

 
  /**
   * Attempt to serve the route requested, looking through all registered api definitions and serving the 
   * appropriate result.
   * 
   * TODO: Handle subpaths
   */
  public function serve() {
    
    header('Content-type: application/json');
    $this->originHeaders();
    
    try {
      
      $context = new RequestContext();
      $context->fromRequest(Request::createFromGlobals());
      
      // Build matcher
      $matcher = new UrlMatcher(
              $this->rootCollection,
              $context
      );
      
      
      // Serve route
      $request_method = strtoupper($_SERVER['REQUEST_METHOD']);
      
      $parameters = $matcher->match(rtrim($context->getPathInfo(), '/'));
      
      if (class_exists($parameters['_controller'])) {
        $page = new $parameters['_controller']();
        
        if (!$page instanceof RESTEndpoint) {
          throw new \RuntimeException("Controller {$parameters['_controller']} must extend RESTEndpoint");
        }
        
        if (!$page instanceof HTTPVerb) {
          throw new \RuntimeException("Controller {$parameters['_controller']} must implement one or more HTTP Verbs");
        }
        
        if (is_callable([$page, $request_method])) {

          // Execute method call
          $result = call_user_func([$page, $request_method], $parameters);

          // If there is a data filter for the output, then send results there
          if (is_callable([$page, $request_method . '_filter'])) {

            $result = call_user_func([$page, $request_method . '_filter'], $result, $parameters);

          }

          echo json_encode($result, JSON_PRETTY_PRINT);
          
        } else {
          throw new \RuntimeException("Method $request_method not supported");
        }
        
      } else {
        throw new \RuntimeException("Controller {$parameters['_controller']} not found");
      }

      
    } catch (ResourceNotFoundException $rex) {
      
      http_response_code(404);
      
      echo json_encode([
          'error' => [
            'code' => 404,
            'message' => $rex->getMessage(),
           ]
      ], JSON_PRETTY_PRINT);
      
    } catch (UnauthorizedException $ux) {
      
      http_response_code(401);
      
      echo json_encode([
          'error' => [
            'code' => 401,
            'message' => $ux->getMessage()
           ]
      ], JSON_PRETTY_PRINT);
      
    } catch (SecurityException $sx) {
      
      http_response_code(403);
      
      echo json_encode([
          'error' => [
            'code' => 403,
            'message' => $sx->getMessage()
           ]
      ], JSON_PRETTY_PRINT);
      
    } catch (\Throwable $ex) {
      
      http_response_code(500);
      
      echo json_encode([
          'error' => [
            'code' => 500,
            'message' => $ex->getMessage(),
            'line' => $ex->getLine(),
            'file' => $ex->getFile(),
            'trace' => $ex->getTrace()
           ]
      ], JSON_PRETTY_PRINT);

    }
    
    exit;
    
  }

  /**
   * Return a singleton of the router handler
   * @return \ARIA\REST\router
   */
  public static function router(): router {
    if (!empty(self::$router))
      return self::$router;

    self::$router = new router();

    return self::$router;
  }

}
