<?php


class EndpointTest extends \PHPUnit\Framework\TestCase {
  
  function endpointProvider() {
    
    return [
        
        'basic' => [
            'localhost:8080/api/test/bl7',
            []
        ]
        
    ];
    
  }
  
  function complexProvider() {
    
    return [
        
        'json' => [
            'localhost:8080/api/echo',
            [
               'json' => [ 
                  'foo' => 'bar',
                  'wibble' => 'wobble',
                  'wubblelubble' => 'dub dub'
               ]
            ]
        ]
        
    ];
    
  }
  
  /**
   * @dataProvider endpointProvider
   */
  function testGET($endpoint, $request) {
    
    $client = new \GuzzleHttp\Client();
    $response = $client->request('GET', $endpoint, $request);
    
    
    $status = $response->getStatusCode();
    $body = json_decode($response->getBody());
    
    $this->assertEquals($status, 200);
    $this->assertNotEmpty($body);
    
    
  }
  
  /**
   * @dataProvider complexProvider
   */
  function testJSONPost($endpoint, $request) {
    
    $client = new \GuzzleHttp\Client();
    try {
      $response = $client->request('POST', $endpoint, $request);
    } catch (\GuzzleHttp\Exception\ServerException $e) {
      print_r($e->getResponse()->getBody()->getContents());
      throw $e;
    }
    
    $status = $response->getStatusCode();
    $body = json_decode($response->getBody(), true);
    print_r($body);
    $this->assertEquals($status, 200);
    $this->assertNotEmpty($body);
    
    
    foreach ($request['json'] as $key => $value) {
      $this->assertEquals($body[$key], $value);
    }
  }
  
}