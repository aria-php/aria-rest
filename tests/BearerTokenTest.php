<?php

class BearerTokenTest extends \PHPUnit\Framework\TestCase {
  
  function endpointProvider() {
    
    return [
        
        'basic' => [
            'localhost:8080/api/bearerecho',
            'bearer-token-123'
        ]
        
    ];
    
  }
  
  /**
   * @dataProvider endpointProvider
   */
  public function testBearer($endpoint, $token) {
    $client = new \GuzzleHttp\Client();
    $response = $client->request('GET', $endpoint, [
        'headers' => [
            'Authorization' => 'Bearer ' . $token,  
        ]
    ]);
    
    $result = json_decode($response->getBody(), true);
    $this->assertEquals($result, $token);
  }
  
  /**
   * @dataProvider endpointProvider
   */
  public function testAccessToken($endpoint, $token) {
    $client = new \GuzzleHttp\Client();
    $response = $client->request('GET', $endpoint . "?access_token={$token}");
    
    $result = json_decode($response->getBody(), true);
    $this->assertEquals($result, $token);
  }
}