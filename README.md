# REST API library

This library provides a (hopefully) simple way of defining an API. It supports multiple HTTP methods and versioning.


## Installation

Add the repository to your composer.json:

```
"repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/aria-php/aria-rest.git"
    }
  ],
```

and then `composer require aria-php/aria-rest` from within your project.

## Usage


Broadly speaking it is a highly simplified wrapper around Symfony routing, specifically modified to provide a very simple way of exposing 
page based HTTP methods. 

You start by exposing individual HTTP methods on an endpoint page (GET / POST / etc), and then adding this endpoint page to an APIDefinition collection. This collection adds an optional prefix to 
a given set of api endpoints - typically this will be used to support versioning, and/or to namespace your particular submodule's api somewhere sensible.

You can look at `example/index.php` for a working example of defining an API endpoint and assigning it different HTTP methods to support.


## Setting up your endpoint

In order for the library to work, you need to do a couple of things (which for ARIA have already been done, but check).

* First you need to modify your .htaccess file to point to a PHP file that contains calls to the API route runner (see example/index.php) for a given url.

```
<IfModule mod_rewrite.c>
    RewriteEngine on
    #RewriteBase /

    RewriteCond %{HTTP:Authorization} ^(.*)
    RewriteRule .* - [e=HTTP_AUTHORIZATION:%1]

    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond $1 !^(index\.php)
    RewriteRule ^(.*)$ /index.php/$1 [L]
</IfModule>
```

* This route runner basically contains:

```
// Serve the routes
$router = router::router();
$router->serve();
``` 


## Creating your API endpoint

* First, create your endpoint by creating a class which implements one or more of the HTTP request methods (e.g. `GETEndpoint` or `POSTEndpoint`). These methods should return a value, of either a simple type or an object implementing JsonSerializable that will be returned by the API request.

```
class MyEndpoint implements GETEndpoint {
    public function GET(array $args = []) {
        return [
            'foo' => 'bar'
        ];
    }
}
```

* Next, add your endpoints to an api definition. An API Definition is a grouping of a set of endpoints which hang off a specific versioned endpoint.
```

$definition = new APIDefinition('namespace');
$definition->addRoute('myapi/myendpoint/', MyEndpoint::class);

```

You can optionally specify a version for this endpoint:

```
$definition = new APIDefinition('namespace', 'v1');
$definition->addRoute('myapi/myendpoint/', MyEndpoint::class);
```

* Finally, publish your API to the central router object

```
$router = router::router();

$router->addAPI($definition);
```

## Handling variables

Within your page handler VERB endpoint, you have two possible ways of getting variables. 

The first is the `$args` line which contains variables extracted from the url line as defined by your route handler. The second is to get GET/POST/PUT/PATCH variables using the `->request()` method.

e.g.

```
class MyEndpoint implements GETEndpoint {
    public function GET(array $args = []) {
        return [
            'foo' => $this->request('bar') // Return the contents of ?bar= from the GET line
        ];
    }
}
```

